package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setAccount(request.getParameter("account"));
            user.setName(request.getParameter("name"));
            user.setPassword(request.getParameter("password"));
            user.setBranch(Integer.parseInt(request.getParameter("branch")));
            user.setPosition(Integer.parseInt(request.getParameter("position")));

            new UserService().register(user);

            response.sendRedirect("/Sato_ayaka/top.jsp");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String account = request.getParameter("account");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String confPass = request.getParameter("confPass");

        if (StringUtils.isEmpty(account) == true) {
            messages.add("ログインIDを入力してください");
        } else if (!account.matches("[a-zA-Z0-9]{6,20}")) {
        	messages.add("ログインIDは半角英数字かつ6字以上20字以内で設定してください");
        }
        
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        } else if (!password.matches("[ -~]{6,20}")) {
        	messages.add("パスワードは半角英数字・記号かつ6字以上20字以内で設定してください");
        } else if (!password.equals(confPass)) {
        	messages.add("パスワードが一致しません");
        }
        
        if (name.length() >= 10) {
        	messages.add("名称は10字以内で設定してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}