<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${loginUser.account}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="settings" method="post"><br />
                <input name="id" value="${editUser.id}" id="id" type="hidden"/>

                <label for="account">アカウント名</label>
                <input name="account" value="${editUser.account}" /><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/><br />

                <label for="name">名称</label>
                <input name="name" value="${editUser.name}" id="name"/><br />

				<label for="branch">支店</label>
                <input name="branch" type="${editUser.branch}" id="branch"/><br />

                <label for="position">部署・役職</label>
                <input name="position" type="${editUser.position}" id="position"/><br />

                <input type="submit" value="登録" /> <br />
                <a href="./">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)Your Name</div>
        </div>
    </body>
</html>